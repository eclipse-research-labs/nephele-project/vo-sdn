#!/bin/bash

# Check if a hostname argument is provided, otherwise default to "localhost"
if [ -n "$1" ]; then
    HOSTNAME="$1"
else
    HOSTNAME="localhost"
fi

cd /home/developer/SD-MIoT/control-plane/SD-MIoT-to-VERO-SDN_Adapters/sd-miot-to-vero-sdn-adapter-COOJA-runtime/

# passing nephele-controller hostname for SDN controller
sudo ./sd-miot-to-vero-sdn-adapter $HOSTNAME
