#!/bin/bash

# Check if a hostname argument is provided
if [ -n "$1" ]; then
  # replace localhost in node-red flow with correct host name
  #sed -i 's/"host": "localhost"/"host": "nephele-controller"/g' /home/developer/.node-red/flows.json
  sed -i 's/localhost/nephele-controller/g' /home/developer/.node-red/flows.json
fi

# execute node-red and pass settings.js changes
node-red --define httpStatic='/home/developer/.node-red/public/' --define httpAdminRoot='/admin'
