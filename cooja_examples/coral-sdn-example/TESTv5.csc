<?xml version="1.0" encoding="UTF-8"?>
<simconf>
  <project EXPORT="discard">[APPS_DIR]/mrm</project>
  <project EXPORT="discard">[APPS_DIR]/mspsim</project>
  <project EXPORT="discard">[APPS_DIR]/avrora</project>
  <project EXPORT="discard">[APPS_DIR]/serial_socket</project>
  <project EXPORT="discard">[APPS_DIR]/collect-view</project>
  <project EXPORT="discard">[APPS_DIR]/powertracker</project>
  <project EXPORT="discard">[APPS_DIR]/mobility</project>
  <project EXPORT="discard">[APPS_DIR]/serial2pty</project>
  <simulation>
    <title>My simulation</title>
    <speedlimit>1.0</speedlimit>
    <randomseed>123456</randomseed>
    <motedelay_us>1000000</motedelay_us>
    <radiomedium>
      org.contikios.cooja.radiomediums.UDGM
      <transmitting_range>110.0</transmitting_range>
      <interference_range>110.0</interference_range>
      <success_ratio_tx>1.0</success_ratio_tx>
      <success_ratio_rx>1.0</success_ratio_rx>
      <transmitting_range_lr>260.0</transmitting_range_lr>
      <interference_range_lr>260.0</interference_range_lr>
      <success_ratio_tx_lr>1.0</success_ratio_tx_lr>
      <success_ratio_rx_lr>1.0</success_ratio_rx_lr>
    </radiomedium>
    <events>
      <logoutput>40000</logoutput>
    </events>
    <motetype>
      org.contikios.cooja.contikimote.ContikiMoteType
      <identifier>mtype826</identifier>
      <description>Cooja Mote Type #1</description>
      <source>[CONTIKI_DIR]/examples/coral-sdn-example/node_br.c</source>
      <commands>make node_br.cooja TARGET=cooja</commands>
      <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Battery</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiVib</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiMoteID</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRS232</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiBeeper</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiIPAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadio</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadioLR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiButton</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPIR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiClock</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiLED</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiCFS</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiEEPROM</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPowerTracker</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>
      <symbols>false</symbols>
    </motetype>
    <motetype>
      org.contikios.cooja.contikimote.ContikiMoteType
      <identifier>mtype642</identifier>
      <description>Cooja Mote Type #2</description>
      <source>[CONTIKI_DIR]/examples/coral-sdn-example/node.c</source>
      <commands>make node.cooja TARGET=cooja</commands>
      <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Battery</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiVib</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiMoteID</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRS232</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiBeeper</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiIPAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadio</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadioLR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiButton</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPIR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiClock</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiLED</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiCFS</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiEEPROM</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPowerTracker</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>
      <symbols>false</symbols>
    </motetype>
    <motetype>
      org.contikios.cooja.contikimote.ContikiMoteType
      <identifier>mtype181</identifier>
      <description>Cooja Mote Type #3</description>
      <source>[CONTIKI_DIR]/examples/coral-sdn-example/vnode.c</source>
      <commands>make vnode.cooja TARGET=cooja</commands>
      <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Battery</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiVib</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiMoteID</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRS232</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiBeeper</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiIPAddress</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadio</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiRadioLR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiButton</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPIR</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiClock</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiLED</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiCFS</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiEEPROM</moteinterface>
      <moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiPowerTracker</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>
      <symbols>false</symbols>
    </motetype>
    <mote>
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>50.0</x>
        <y>0.0</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiMoteID
        <id>1</id>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadio
        <bitrate>250.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadioLR
        <bitrate>50.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiEEPROM
        <eeprom>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</eeprom>
      </interface_config>
      <motetype_identifier>mtype826</motetype_identifier>
    </mote>
    <mote>
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>-65.692574</x>
        <y>-139.64624</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiMoteID
        <id>2</id>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadio
        <bitrate>250.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadioLR
        <bitrate>50.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiEEPROM
        <eeprom>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</eeprom>
      </interface_config>
      <motetype_identifier>mtype642</motetype_identifier>
    </mote>
    <mote>
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>85.613396</x>
        <y>-148.055511</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiMoteID
        <id>3</id>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadio
        <bitrate>250.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadioLR
        <bitrate>50.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiEEPROM
        <eeprom>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</eeprom>
      </interface_config>
      <motetype_identifier>mtype642</motetype_identifier>
    </mote>
    <mote>
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>-146.63002</x>
        <y>119.423759</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiMoteID
        <id>4</id>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadio
        <bitrate>250.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadioLR
        <bitrate>50.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiEEPROM
        <eeprom>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</eeprom>
      </interface_config>
      <motetype_identifier>mtype642</motetype_identifier>
    </mote>
    <mote>
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>-78.188858</x>
        <y>-119.600639</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiMoteID
        <id>5</id>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadio
        <bitrate>250.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiRadioLR
        <bitrate>50.0</bitrate>
      </interface_config>
      <interface_config>
        org.contikios.cooja.contikimote.interfaces.ContikiEEPROM
        <eeprom>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</eeprom>
      </interface_config>
      <motetype_identifier>mtype642</motetype_identifier>
    </mote>
  </simulation>
  <plugin>
    org.contikios.cooja.plugins.SimControl
    <width>288</width>
    <z>6</z>
    <height>160</height>
    <location_x>622</location_x>
    <location_y>2</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.Visualizer
    <plugin_config>
      <moterelations>true</moterelations>
      <skin>org.contikios.cooja.plugins.skins.IDVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.GridVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.TrafficVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.UDGMVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.UDGMLongRangeVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.MoteTypeVisualizerSkin</skin>
      <viewport>1.4390825348482414 0.0 0.0 1.4390825348482414 223.37516568406292 320.7373019369951</viewport>
    </plugin_config>
    <width>623</width>
    <z>4</z>
    <height>715</height>
    <location_x>1</location_x>
    <location_y>1</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.LogListener
    <plugin_config>
      <filter />
      <formatted_time />
      <coloring />
    </plugin_config>
    <width>656</width>
    <z>5</z>
    <height>204</height>
    <location_x>624</location_x>
    <location_y>161</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.TimeLine
    <plugin_config>
      <mote>0</mote>
      <mote>1</mote>
      <mote>2</mote>
      <mote>3</mote>
      <mote>4</mote>
      <showRadioRXTX />
      <showRadioHW />
      <showLEDs />
      <zoomfactor>500.0</zoomfactor>
    </plugin_config>
    <width>635</width>
    <z>12</z>
    <height>166</height>
    <location_x>403</location_x>
    <location_y>446</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.Notes
    <plugin_config>
      <notes>1 BR
15 (2-16) Vehicles
16 (17-32) Static</notes>
      <decorations>true</decorations>
    </plugin_config>
    <width>203</width>
    <z>8</z>
    <height>89</height>
    <location_x>909</location_x>
    <location_y>-1</location_y>
  </plugin>
  <plugin>
    de.fau.cooja.plugins.Serial2Pty
    <mote_arg>0</mote_arg>
    <plugin_config />
    <width>171</width>
    <z>9</z>
    <height>86</height>
    <location_x>1112</location_x>
    <location_y>0</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.MoteInterfaceViewer
    <mote_arg>0</mote_arg>
    <plugin_config>
      <interface>Serial port</interface>
      <scrollpos>0,0</scrollpos>
    </plugin_config>
    <width>595</width>
    <z>11</z>
    <height>193</height>
    <location_x>2</location_x>
    <location_y>662</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.ScriptRunner
    <plugin_config>
      <script>// 6 nodes  30 min 1800000&#xD;
// 10 nodes 50 min 3000000&#xD;
// 15 nodes 62 min 3800000&#xD;
TIMEOUT(900000);&#xD;
timeout_function = function () {&#xD;
    log.log("\nPackets Sent\n=========================\n");	&#xD;
    for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      log.log(packetsSent[i][j]+",");&#xD;
	    }&#xD;
	    log.log("\n");&#xD;
	}    &#xD;
&#xD;
    log.log("\nPackets Received\n=========================\n");	&#xD;
    for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      log.log(packetsReceived[i][j]+",");&#xD;
	    }&#xD;
	    log.log("\n");&#xD;
	}      &#xD;
            &#xD;
	log.log("\nPDR\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
          if(packetsSent[i][j] != 0)&#xD;
	         log.log(packetsReceived[i][j]/packetsSent[i][j]*100+",");&#xD;
          else&#xD;
             log.log("0,");&#xD;
	    }&#xD;
	    log.log("\n");&#xD;
	}&#xD;
&#xD;
	log.log("\nTOTAL delay\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      log.log(delaySUM[i][j]+",");&#xD;
	   }&#xD;
	   log.log("\n");&#xD;
	}&#xD;
&#xD;
	log.log("\nMAX delay\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      log.log(delayMAX[i][j]+",");&#xD;
	   }&#xD;
	   log.log("\n");&#xD;
	}&#xD;
&#xD;
	log.log("\nMIN delay\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      log.log(delayMIN[i][j]+",");&#xD;
	   }&#xD;
	   log.log("\n");&#xD;
	}&#xD;
	&#xD;
	log.log("\nAVERAGE delay all nodes to all\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
	      if(i!=j &amp;&amp; packetsReceived[i][j]!=0)&#xD;
	         log.log(delaySUM[i][j]/packetsReceived[i][j]+",");&#xD;
	      else&#xD;
	         log.log("0,");&#xD;
	   }&#xD;
	   log.log("\n");&#xD;
	}&#xD;
	log.log("\nSD delay all nodes to all\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
          if(i!=j &amp;&amp; packetsReceived[i][j]!=0) {&#xD;
             avg = delaySUM[i][j]/packetsReceived[i][j]&#xD;
             sumSD = 0;&#xD;
 	         for(z = 1; z &lt; packetsReceived[i][j]; z++) {&#xD;
	            sumSD = sumSD + Math.pow((delayALL[i][j][z] - avg),2);&#xD;
             }&#xD;
             log.log(Math.sqrt(sumSD/packetsReceived[i][j])+",");&#xD;
          }&#xD;
          else {&#xD;
            log.log("0,");&#xD;
          }&#xD;
	   }&#xD;
	   log.log("\n");&#xD;
	}&#xD;
&#xD;
/*	log.log("\nAVERAGE delay per node to all\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
       Dsum = 0; //Delay sum&#xD;
       Psum = 0; //Packets sum&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
          Dsum = Dsum + delaySUM[i][j];&#xD;
          Psum = Psum + packetsReceived[i][j];&#xD;
       }&#xD;
       if(Psum!=0)&#xD;
	      log.log(Dsum/Psum+",");&#xD;
	   else&#xD;
	      log.log("0,");&#xD;
	}*/&#xD;
	log.log("\nAVG,SD delay per node to all\n=========================\n");&#xD;
	for(i = 1; i &lt; maxNodes; i++) {&#xD;
       Dsum = 0; //Delay sum&#xD;
       Psum = 0; //Packets sum&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
          Dsum = Dsum + delaySUM[i][j];&#xD;
          Psum = Psum + packetsReceived[i][j];&#xD;
       }&#xD;
       if(Psum!=0) {&#xD;
          Davg = Dsum / Psum;&#xD;
	      log.log(Davg+",");&#xD;
          // Find SD&#xD;
          sumSD = 0;&#xD;
	      for(j = 1; j &lt; maxNodes; j++) {&#xD;
             sumSD = sumSD + Math.pow((delaySUM[i][j] - Davg),2);&#xD;
          }&#xD;
          log.log(Math.sqrt(sumSD/Psum)+"\n");&#xD;
       }&#xD;
	   else&#xD;
	      log.log("0\n");&#xD;
	}&#xD;
    log.log("\n");&#xD;
&#xD;
	log.log("\nTOTAL Delay, AVG Delay Overall Network per packet\n=========================\n");&#xD;
    for(z = 1; z &lt;= packetsReceived[1][2]; z++) {&#xD;
       sumALL = 0;&#xD;
       num = 0;&#xD;
       for(i = 1; i &lt; maxNodes; i++) {&#xD;
	      for(j = 1; j &lt; maxNodes; j++) {&#xD;
             if(i!=j &amp;&amp; packetsReceived[i][j]!=0) {&#xD;
	            sumALL = sumALL + delayALL[i][j][z];&#xD;
                num++;&#xD;
             }&#xD;
          }&#xD;
       }&#xD;
       log.log(z+","+sumALL+","+sumALL/num+"\n");&#xD;
	}&#xD;
    log.log("\n");&#xD;
&#xD;
	log.log("\nDelay Per PATH, AVG Delay Per Path for all packets\n=========================\n");&#xD;
    for(i = 1; i &lt; maxNodes; i++) {&#xD;
       for(j = 1; j &lt; maxNodes; j++) {&#xD;
          if(i!=j &amp;&amp; packetsReceived[i][j]!=0) {&#xD;
             sumALL = 0;&#xD;
             num = 0;&#xD;
             for(z = 1; z &lt;= packetsReceived[1][2]; z++) {&#xD;
	            sumALL = sumALL + delayALL[i][j][z];&#xD;
                num++;&#xD;
             }&#xD;
             log.log(i+"-&gt;"+j+","+sumALL+","+sumALL/num+"\n");&#xD;
          }&#xD;
       }&#xD;
	}&#xD;
    log.log("\n");&#xD;
&#xD;
	log.log("\nTOTAL Delay, AVG Delay Overall Network per packet\n=========================\n");&#xD;
    for(i = 1; i &lt; maxNodes; i++) {&#xD;
	   for(j = 1; j &lt; maxNodes; j++) {&#xD;
          if(i!=j &amp;&amp; packetsReceived[i][j]!=0) {&#xD;
             log.log(","+i+"-&gt;"+j);&#xD;
          } &#xD;
       }&#xD;
    }&#xD;
    for(z = 1; z &lt;= packetsReceived[1][2]; z++) {&#xD;
       log.log("\n"+z);&#xD;
       for(i = 1; i &lt; maxNodes; i++) {&#xD;
	      for(j = 1; j &lt; maxNodes; j++) {&#xD;
             if(i!=j &amp;&amp; packetsReceived[i][j]!=0) {&#xD;
                log.log(","+delayALL[i][j][z]);&#xD;
             }&#xD;
          }&#xD;
       }&#xD;
	}&#xD;
    log.log("\n");&#xD;
&#xD;
    log.testOK();&#xD;
}&#xD;
&#xD;
Motes = sim.getMotes();&#xD;
maxNodes = Motes.length+1;&#xD;
maxMessages = 200; // Maximium messages send per node&#xD;
log.log("Maxnodes=" + maxNodes + "\n");&#xD;
&#xD;
log.log("Initialization start... \n");&#xD;
packetsReceived= new Array(maxNodes);&#xD;
packetsSent = new Array(maxNodes);&#xD;
timeReceived= new Array(maxNodes);&#xD;
timeSent = new Array(maxNodes);&#xD;
delaySUM = new Array(maxNodes);&#xD;
delayMIN = new Array(maxNodes);&#xD;
delayMAX = new Array(maxNodes);&#xD;
delayALL = new Array(maxNodes);&#xD;
&#xD;
for(i = 0; i &lt; maxNodes; i++) {&#xD;
   packetsReceived[i] = new Array(maxNodes);&#xD;
   packetsSent[i] = new Array(maxNodes);&#xD;
   timeReceived[i] = new Array(maxNodes);&#xD;
   timeSent[i] = new Array(maxNodes);&#xD;
   delaySUM[i] = new Array(maxNodes);&#xD;
   delayMIN[i] = new Array(maxNodes);&#xD;
   delayMAX[i] = new Array(maxNodes);&#xD;
   delayALL[i] = new Array(maxNodes);&#xD;
   for(j = 0; j &lt; maxNodes; j++) {&#xD;
      packetsReceived[i][j] = 0;&#xD;
      packetsSent[i][j] = 0;&#xD;
      timeReceived[i][j] = 0;&#xD;
      timeSent[i][j] = 0;&#xD;
      delaySUM[i][j] = 0;&#xD;
      delayMIN[i][j] = 100000;&#xD;
      delayMAX[i][j] = 0;    &#xD;
      delayALL[i][j] = new Array(maxMessages);       &#xD;
      for(z = 0; z &lt; maxMessages; z++) {&#xD;
         delayALL[i][j][z] = 0;             &#xD;
      }&#xD;
   }&#xD;
}&#xD;
log.log("Initialization end...\n");&#xD;
&#xD;
while(1) {&#xD;
   YIELD();&#xD;
   msgArray = msg.split(' ');&#xD;
   if(msgArray[0].equals("#RECV")) {&#xD;
         // Received packet&#xD;
         Sid = parseInt(msgArray[3]);&#xD;
         Rid = parseInt(msgArray[1]);&#xD;
      &#xD;
         packetsReceived[Sid][Rid]++;&#xD;
         timeReceived[Sid][Rid] = time;&#xD;
         delay = timeReceived[Sid][Rid] - timeSent[Sid][Rid];&#xD;
         delaySUM[Sid][Rid] = delaySUM[Sid][Rid] + delay;&#xD;
         if(delayMAX[Sid][Rid] &lt; delay){&#xD;
            delayMAX[Sid][Rid] = delay;  &#xD;
         }&#xD;
         if(delayMIN[Sid][Rid] &gt; delay){&#xD;
            delayMIN[Sid][Rid] = delay;&#xD;
         }&#xD;
         delayALL[Sid][Rid][packetsReceived[Sid][Rid]] = delay;             &#xD;
         log.log("Recv " + Rid +" from " + Sid);&#xD;
         log.log(" SentT=" + timeSent[Sid][Rid] + " RevcT=" + timeReceived[Sid][Rid]);&#xD;
         log.log(" Delay=" + delay + " delaySUM=" + delaySUM[Sid][Rid]);&#xD;
         log.log(" MAX=" + delayMAX[Sid][Rid] + " MIN=" + delayMIN[Sid][Rid] + "\n");&#xD;
      } else if(msgArray[0].equals("#SEND")) {&#xD;
         // Sent packet&#xD;
         Sid = parseInt(msgArray[1]);&#xD;
         Rid = parseInt(msgArray[3]);&#xD;
         packetsSent[Sid][Rid]++;&#xD;
         timeSent[Sid][Rid] = time;&#xD;
         log.log("Send"  + Sid + " to " + Rid + " timeSent=" + timeSent[Sid][Rid] +"\n");&#xD;
      }&#xD;
}</script>
      <active>true</active>
    </plugin_config>
    <width>657</width>
    <z>7</z>
    <height>353</height>
    <location_x>623</location_x>
    <location_y>360</location_y>
  </plugin>
  <plugin>
    Mobility
    <plugin_config>
      <positions EXPORT="copy">[APPS_DIR]/mobility/1h15v400x400.dat</positions>
    </plugin_config>
    <width>372</width>
    <z>10</z>
    <height>76</height>
    <location_x>908</location_x>
    <location_y>85</location_y>
  </plugin>
</simconf>

