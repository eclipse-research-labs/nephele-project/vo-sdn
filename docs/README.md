# Design of VO Reactive Routing component

## Table of Contents

- [Introduction](#introduction)
- [SDN Control Plane](#sdn-control-plane)
- [SDN Data Plane](#sdn-data-plane)
- [Interactions between SDN and Data Planes](#interactions-between-sdn-and-data-planes)

## Introduction

Here, we provide the basic details of Reactive Routing component design. As shown in the following figure, the (c)VOs implement Reactive Routing through the following SDN control facilities: (i) Topology Discovery, (ii) Network Model, (iii)
Clustering, and (iv) Path Computation, which we detail below. Furthermore, we also briefly elaborate on the details of SDN data plane and the basic interactions between control and data planes.

![Reactive Routing Architecture](https://raw.githubusercontent.com/SWNRG/nephele-sdiot/57da9071fb072999ad4e08d9a91980d405d81e1c/docs/images/PhysicalConvergenceLayerOfVOStack_processed.png?token=GHSAT0AAAAAACLEWJ2ALUBT5QOCUAY36ZL6ZLWXVXA)

## SDN Control Plane
The Compute Continuum Network manager implements high-level network management functionalities. The composite Virtual Object (cVO) translates such requirements into a customised protocol configuration for a number of Virtual Objects (VOs). Those VOs, in their turn, implement autonomous network control features.

Basically, the VOs receive high-level configuration options and directives being decided from the components above them (i.e., cVO or Compute Continuum Network Manager), configuring or adapting their autonomic mechanisms, so they perform in alignment to the requirements of the IoT application.
  
### Topology Discovery (TD)
TD is implemented in each VO and can have custom-made configuration. Three implementation mechanisms are supported:
  
1. *TC-NA* - TC-NA is an epidemic algorithm. TD periodically communicates topology discovery control packets to its assigned BR. The BR then broadcasts a “Neighbors’ Discovery” short-range beacon message. Each receiving neighbour creates a response message that informs TD for any link existence between the beacon node and itself.
  
2. *TC-NR* - TC-NR is a centralised topology discovery algorithm. It collects topology information through individual requests to the nodes from the TD and can send targeted topology requests on specific nodes, or parts of the network, without overloading the rest of the topology.
  
3. *TC-NA / TC-NR* - Finally, this is a hybrid combination of the other two algorithms. It is used for heterogeneous topologies that consist of both mobile and fixed nodes.

### Network Model (NM)
The NM is responsible for topology maintenance and representation. The topology maintenance is fully coordinated from the VO. The topology representation assigns frequently updated link quality values to each edge of the graph. It resides both at VOs and the cVO. cVO also supports a graphical representation of the topology.

A screenshot on a corresponding cVO GUI follows.

![cVO GUI](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-sdn/-/raw/e051ac0dcb639ede9bd506249e025c8294decbd1/docs/images/cVOgui.png)

### Path Computation (PC)
PC specifies the end-to-end paths from source to destination nodes. These paths should be aligned to the requirements of the IoT application. It determines these paths that are being translated to flow rules in tuples of Destination and Next Hop node addresses, being stored to the individual flow table of each node. 

There are four types of flow establishment processes.
  
1. *Next-Hop Only (NHO)* - NHO communicates to the nodes their own forwarding rule only.
  
2. *Complete Path (CP)* - CP informs all the intermediate nodes participating in the routing path.
  
3. *NHO-CP* - An NHO-CP combination is being employed in the case of topologies consisting of both mobile (NHO) and fixed (CP) nodes.
  
4. *Proactive Flow Establishment (PFE)* - PFE employs clustering to classify links based on their connectivity quality history, which guides proactive flow establishment rules.

### Clustering
Clustering capabilities are being supported at the level of cVO or Compute Continuum Network Manager.

## SDN Data Plane

The IoT nodes have two radio interfaces. One of them supports long-range lower-speed communication, and the other short-range high-speed communication. The long-range interface is used for the SDN control channel, while the short-range one is used for data communication.

One border router (BR) is located at the IoT Gateway. The architecture can support the existence of multiple BRs.

There are two network stacks installed in each IoT node. One for each interface. The control network stack handles long-range communication and SDN control messages & processes. The data network stack handles low-power, short-range wireless communication and the forwarding layer of the SDN protocol.

## Interactions between SDN Control and Data Plane

### Southbound API 

![Southbound API](https://raw.githubusercontent.com/SWNRG/nephele-sdiot/57da9071fb072999ad4e08d9a91980d405d81e1c/docs/images/southboundAPI.png?token=GHSAT0AAAAAACLEWJ2BFDDQSAEYR2LQI5BWZLWXVHA)

Southbound API manages control messages exchanged between the SDN Controller and the IoT nodes. The API’s messages can belong to one of the following categories:

1. *Topology Control Messages* - they concern the basic functionalities of topology discovery and maintenance.
  
2. *Routing Control Messages* - messages related to the establishment of paths among node devices.
  
3. *Data Delivery Control Messages* - messages related to IoT data delivery.
