# VO SDN Control Plane

This repository contains the prototype implementation of Reactive Routing component of VO stack. Reactive Routing maintains network connectivity between wireless IoT nodes and the IoT gateway. We adopt the Software-Defined Wireless Sensor Networking (SD-WSN) approach, which enables dynamic routing adjustments based on a global view of the network, e.g., handling routing changes due to mobility or signal interference. Furthermore, it provides logically-centralized and programmable routing control, allowing easy integration with the distributed control across the compute continuum, fostering enhanced network intelligence and monitoring capabilities

A screenshot on a corresponding cVO GUI follows.

![cVO GUI](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-sdn/-/raw/e051ac0dcb639ede9bd506249e025c8294decbd1/docs/images/cVOgui.png)

## Documentation

Basic VO-SDN design details can be found [here](docs).

## References

Please cite the following paper, if you use the code:
- G. Papathanail, L. Mamatas, T. Theodorou, I Sakellariou, P. Papadimitriou, N. Filinis, D. Spatharakis, E. Fotopoulou, A. Zafeiropoulos, S. Papavassiliou, A Virtual Object Stack for IoT-Enabled Applications Across the Compute Continuum, IEEE/ACM UCC CEICO 2023.

Other relevant works:
- T. Theodorou and L. Mamatas, "SD-MIoT: A Software-Defined Networking Solution for Mobile Internet of Things," in IEEE Internet of Things Journal, vol. 8, no. 6, pp. 4604-4617, 15 March, 2021, doi: 10.1109/JIOT.2020.3027427.
- T. Theodorou and L. Mamatas, "A Versatile Out-of-Band Software-Defined Networking Solution for the Internet of Things," in IEEE Access, vol. 8, pp. 103710-103733, 2020, doi: 10.1109/ACCESS.2020.2999087.
- T. Theodorou, G. Violettas, P. Valsamas, S. Petridou and L. Mamatas, "A Multi-Protocol Software-Defined Networking Solution for the Internet of Things," in IEEE Communications Magazine, vol. 57, no. 10, pp. 42-48, October 2019, doi: 10.1109/MCOM.001.1900056.

## Quickstart

SD-MIoT represents an emulation of an IoT network via cooja, the gateway adaptor the IoT gateway, the SDN controller the reactive routing VO component and the cVO GUI is implemented as Node-RED flows.

We first need to clone the VO-SDN repository:

```console
git clone https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-sdn.git
```

The reactive routing prototype can either be executed manually or via docker compose. We give brief instructions for both options below.

### Manual execution of VO-SDN

We can execute the cooja GUI with:

```console
./cooja-gui.sh
```

We can manually open nephele.csc from cooja GUI, which implements and example IoT topology. The same file can be adapted, so different topologies can be considered. We can now start the cooja experiment.

We should connect to the same container from another terminal via:

```console
./shell-instance.sh
```

After that, we manually execute the IoT gateway:

```console
./execute_adaptor.sh
```

With a similar process, we execute the reactive routing component in the same container:

```console
./shell-instance.sh
./execute_controller.sh
```

Finally, we run the cVO GUI implemented in the form of node-red flows:

```console
./execute_node-red.sh
```

All components should now be communicating between each other.

### Docker compose execution of VO-SDN

We can run the IoT network, IoT gateway, reactive routing component and cVO GUI via a single docker compose file. We just need to execute:

```console
docker compose pull
docker compose up
```

With `docker compose pull`, we ensure that we have the latest version of container images in our local machine.

Here are the details of docker-compose.yaml file:

```YAML
version: '3'
services:
  nephele-controller:
    image: swnuom/nephele-sdiot
    environment:
      - JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
      - JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
    ports:
      - "8992:8992"
      - "8999:8999"
    command: "/home/developer/execute_controller.sh"
  nephele-sdmiot:
    image: swnuom/nephele-sdiot
    environment:
      - JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
      - JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
    command: "/home/developer/start.sh nephele-controller"
  nephele-nodered:
    image: swnuom/nephele-sdiot
    ports:
      - "1880:1880"
    command: "/home/developer/execute_node-red.sh nephele-controller"
```
