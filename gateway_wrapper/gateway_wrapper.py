import json
import tornado.ioloop
import tornado.web
import tornado.tcpserver
import tornado.iostream

# In-memory storage for controller data structures
configuration = json.loads("{}")
topology = json.loads("{\"network\":{\"topology\":{\"nodes\":[]}}}")
flows = {}

# Store tcp connection
active_tcp_connection = None

# Store border router id
bid = "01.00"

# Store if mobility is enabled
mobility = 0

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Simple SDN controller wrapper")

class ConfigurationHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def get(self):
        global configuration
        self.write(json.dumps(configuration))

    def post(self):
        global configuration
        global mobility
        global bid

        data = json.loads(self.request.body)
        configuration = data

        # converting configuration to SDN controller format
        # - **CMD** : The name of command
        # -- "Start" : To start a new experiment
        # -- "Update" : To update configuration
        # - **SINK** : The value of SINK ID field
        # - **TCT** :
        #  -- 0 : Topology control advertisement based
        #  -- 1 : Topology control node neighbors list based
        # - **ACK** : 
        # -- 0 : Topology control without Acknowledgement
        # -- 1 : Topology control with Acknowledgement
        # - **RET** : The value of  Retransmission delay field. Range 1-10
        # - **ROUT** : The value of  Routing Configuration field
        # -- 0 : Next hop only
        # -- 1 : Total Path
        # - **LQEA** : The value of  Link Quality Estimation field
        # -- 0 : RSSI
        # -- 1 : RSSI & Energy
        # -- 2 : ΕΤΧ
        # -- 3 : JSI intelligent LQE algorithm

        # Handle the message
        mobility = next((item['value'] for item in data["network"]["topology"]["configuration"] if item["parameter"] == "mobility"), None)
        ret = next((item['value'] for item in data["network"]["topology"]["topologycontrol"] if item["parameter"] == "retransmissiondelay"), None)
        tct = next((item['value'] for item in data["network"]["topology"]["topologycontrol"] if item["parameter"] == "method"), None)
        ack = next((item['value'] for item in data["network"]["topology"]["topologycontrol"] if item["parameter"] == "acknowledgements"), None)
        lc_id = data.get("LC_id")

        print ("mobility:"+str(mobility))
        print ("ret:"+str(ret))
        print ("tct:"+tct)
        print ("ack:"+str(ack))

        # validate input
        if mobility == 0 or mobility == 1:
            if ret > 0 and ret < 11:
                if tct == "0" or tct == "1":
                    if ack == 0 or ack == 1: 
                        # input is valid, proceed

                        # pass configuration to SDN controller
                        controller_configuration="{\"RET\":\""+str(ret)+"\",\"PTY\":\"ND\",\"TCT\":\""+tct+"\",\"NID\":\"01.00\",\"ACK\":\""+str(ack)+"\",\"BID\":\""+bid+"\"}"
                        send_message_to_tcp_connection("{\"Msg\":"+controller_configuration+",\"LC_id\":\"01.00\"}")
                        self.set_status(201)
                        self.write({"message": "configuration created"})
                    else:
                        self.set_status(400)
                        self.write({"message": "invalid ack parameter value"})
                else:
                    self.set_status(400)
                    self.write({"message": "invalid tct parameter value"})
            else:
                self.set_status(400)
                self.write({"message": "invalid ret parameter value"})
        else:
            self.set_status(400)
            self.write({"message": "invalid mobility parameter value"})

    def delete(self):
        global configuration
        configuration = json.loads("{}")
        self.write({"message": "configuration deleted"})

class TopologyHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def get(self):
        global topology
        self.write(json.dumps(topology))

    def post(self):
        global topology
        data = json.loads(self.request.body)
        topology = json.loads(data)
        self.set_status(201)
        self.write({"message": "topology created"})

    def delete(self):
        global topology
        topology = json.loads("{\"network\":{\"topology\":{\"nodes\":[]}}}")
        self.write({"message": "topology deleted"})

class FlowHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def get(self, flow_id=None):
        if flow_id:
            flow = flows.get(flow_id, None)
            if flow:
                self.write(json.dumps(flow))
            else:
                self.set_status(404)
                self.write({"error": "Flow not found"})
        else:
            self.write(json.dumps(flows))

    def post(self):
        data = json.loads(self.request.body)
        flow_data = data.get("flows", [])

        created_flows = []
        errors = []

        for flow in flow_data:
            flow_id = flow.get("flowId")
            if flow_id is not None:
                if flow_id in flows:
                    errors.append("Flow with ID {} already exists".format(flow_id))
                else:
                    flows[flow_id] = flow
                    created_flows.append(flow_id)
                    # transmit flows to IoT gateway
                    source=flow.get("source")
                    sink=flow.get("sink")

                    # pass configuration to SDN controller
                    #controller_configuration="{\"PTY\":\"ND\",\"TCT\":\""+tct+"\",\"NID\":\"01.00\",\"ACK\":\""+str(ack)+"\",\"BID\":\""+bid+"\"}"
                    #send_message_to_tcp_connection("{\"Msg\":"+controller_configuration+",\"LC_id\":\"01.00\"}")
                    #self.set_status(201)
                    #self.write({"message": "configuration created"})
            else:
                errors.append("Flow missing flowId")

        if created_flows:
            self.set_status(201)
            self.write({
                "message": "Flows created",
                "created_flows": created_flows
            })
        if errors:
            self.set_status(400)
            self.write({
                "message": "Some flows encountered errors",
                "errors": errors
            })

    def put(self, flow_id):
        if flow_id in flows:
            data = json.loads(self.request.body)
            flows[flow_id] = data
            self.write({"message": "Flow updated", "id": flow_id})
        else:
            self.set_status(404)
            self.write({"error": "Flow not found"})

    def delete(self, flow_id):
        if flow_id in flows:
            del flows[flow_id]
            self.write({"message": "Flow deleted"})
        else:
            self.set_status(404)
            self.write({"error": "Flow not found"})

# Function to check if a particular node ID exists
def node_exists(node_id):
    global topology

    json_data=topology
    nodes = json_data['network']['topology']['nodes']
    for node in nodes:
        if node['id'] == node_id:
            return True
    return False

# Function to update a node's configuration
def update_node_configuration(node_id, new_config):
    global topology

    json_data=topology
    nodes = json_data['network']['topology']['nodes']
    for node in nodes:
        if node['id'] == node_id:
            node.update(new_config)
            print(f"Node {node_id} updated with {new_config}.")
            return
    print(f"Node {node_id} not found.")

# Function to add a new node if the ID doesn't exist
def add_new_node(new_node):
    global topology

    json_data=topology
    nodes = json_data['network']['topology']['nodes']
    if node_exists(new_node['id']):
        print(f"Node with ID {new_node['id']} already exists.")
    else:
        nodes.append(new_node)
        print(f"New node {new_node['id']} added.")

# TCP server for IoT devices
class IoTTCPServer(tornado.tcpserver.TCPServer):
    async def handle_stream(self, stream: tornado.iostream.IOStream, address):
        global active_tcp_connection
        global bid
        global topology

        active_tcp_connection = stream  # Store the active connection

        try:
            while True:
                # Read the incoming message from the IoT device
                message = await stream.read_until(b"\n")
                message = message.decode().strip()
                print ("gateway wrapper received message:" +message)
                # Parse the JSON part
                try:
                    data = json.loads(message)
                        
                    # Process the JSON message (e.g., based on "Msg" or "LC_id")
                    if "Msg" in data and "LC_id" in data:
                        # Determine message type
                        # Examples are:
                        # {"Msg":{"SSS":0,"RSS":100,"PTY":"NB","LQI":151,"NID":"11.00","BID":"01.00","NBR":"10.00","ENG":0},"LC_id":"01.00"}
                        # {"Msg":{"PTY":"BR","BID":"01.00"},"LC_id":"01.00"}

                        if "NID" in data["Msg"]:
                            # Message relates to topology notification

                            # Handle the message
                            # {"Msg":{"SSS":0,"RSS":100,"PTY":"NB","LQI":151,"NID":"11.00","BID":"01.00","NBR":"10.00","ENG":0},"LC_id":"01.00"}
                            sss = data["Msg"].get("SSS")
                            rss = data["Msg"].get("RSS")
                            pty = data["Msg"].get("PTY")
                            lqi = data["Msg"].get("LQI")
                            nid = data["Msg"].get("NID")
                            #bid = data["Msg"].get("BID")
                            nbr = data["Msg"].get("NBR")
                            eng = data["Msg"].get("ENG")
                            lc_id = data.get("LC_id")
                            # update node type
                            if nid==bid:
                                node_type="BR"
                            else:
                                node_type="Sensor"

                            # update topology data structure
                            # check if node exists
                            if node_exists(nid):
                                # update node configuration
                                update_node_configuration(nid, {"id":nid,"type":node_type,"neighbor":nbr,"rssi":rss,"lqi":lqi})
                            else:
                                # add new node configuration
                                new_node = {"id":nid,"type":node_type,"neighbor":nbr,"rssi":rss,"lqi":lqi }
                                add_new_node(new_node)

                            print ("Topology notification message received for node: "+str(nid))
                        elif "BID" in data["Msg"]:
                            # Message relates to border router notification
                            # {"Msg":{"PTY":"BR","BID":"01.00"},"LC_id":"01.00"}

                            # Handle the message
                            msg_type = data["Msg"].get("PTY")
                            bid = data["Msg"].get("BID")
                            lc_id = data.get("LC_id")

                            if msg_type == "BR":
                                # The border router is set
                                print ("Border router is set to "+str(bid))
                                #await stream.write(b"{\"message\": \"Message processed successfully\n\"")
                                #await stream.write (b"{}\n")
                            else:
                                print ("Unknown message type")
                                #await stream.write(b"Unknown message type\n")
                        else:
                            print ("Invalid message format")
                    else:
                        print ("Invalid message format")
                        #await stream.write(b"Invalid message format\n")
                except json.JSONDecodeError:
                    print ("ERROR: Invalid JSON format")
                    #await stream.write(b"ERROR: Invalid JSON format\n")
            else:
                print ("ERROR: Malformed message")
                #await stream.write(b"ERROR: Malformed message\n")

        except tornado.iostream.StreamClosedError:
            print("Connection from {} closed".format(address))
            active_tcp_connection = None

# Helper function to send messages over TCP
def send_message_to_tcp_connection(message):
    global active_tcp_connection
    try:
        stream = active_tcp_connection
        if not stream.closed():
            stream.write((message + "\n").encode())
            print(f"Sent message to TCP client: {message}")
        else:
            print("TCP connection is closed, cannot send message")
    except Exception as e:
        print(f"Error sending message: {e}")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/configuration", ConfigurationHandler),
        (r"/topology", TopologyHandler),
        (r"/flows", FlowHandler),
        (r"/flows/([0-9]+)", FlowHandler),
    ])

if __name__ == "__main__":
    # Start HTTP server
    app = make_app()
    app.listen(8888)
    print("HTTP server running on http://localhost:8888")
    
    # Start TCP server
    tcp_server = IoTTCPServer()
    tcp_server.listen(8999)
    print("TCP server running on port 8999")

    # Start IOLoop
    tornado.ioloop.IOLoop.current().start()
