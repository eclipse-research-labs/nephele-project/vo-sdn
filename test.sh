#!/bin/bash

# check error function, has one parameter: the successful message
check_error() {
    if [ $? -ne 0 ]; then
        echo "An error occurred. Exiting."
        exit 1
    else
	# print message in red
	echo -e "\033[31m$1\033[0m"
    fi
}

# pulling docker compose images
echo "TEST1: Testing pulling docker compose images"
docker compose pull

check_error "VO-SDN images pulled successfully"

echo ""

# executing docker compose up in background
echo "TEST2: Testing docker compose execution of VO-SDN"
docker compose up -d

check_error "Docker compose up have been successfully executed"

echo ""

# break a bit
echo "Waiting 30 seconds"
sleep 30

echo ""

# testing if SDN controller is running
echo "TEST2: Testing SDN controller"
docker exec vo-sdn-nephele-controller-1 ps aux | grep /home/developer/execute_controller.sh

check_error "SDN controller is running"

echo ""

# testing if cooja is running
echo "TEST3: Testing Cooja"
docker exec vo-sdn-nephele-sdmiot-1 ps aux | grep execute_cooja.sh

check_error "Cooja is running"

echo ""

# testing if adaptor is running
echo "TEST4: Testing adaptor"
docker exec vo-sdn-nephele-sdmiot-1 ps aux | grep execute_adaptor.sh

check_error "Adaptor is running"

echo ""

# testing node-red
echo "TEST4: Testing node-red"
docker exec vo-sdn-nephele-nodered-1 ps aux | grep execute_node-red.sh

check_error "NODE-RED is running"

echo ""

# shutting down VO-SDN
echo "TEST2: Shutting down VO-SDN"
docker compose up -d

check_error "Docker compose down have been successfully executed"

echo ""
