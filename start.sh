#!/bin/bash

# Check if a hostname argument is provided, otherwise default to "localhost"
if [ -n "$1" ]; then
    HOSTNAME="$1"
else
    HOSTNAME="localhost"
fi

echo "Starting SD-MIoT infrastructure plane"
echo "Waiting to be ready"

# Starting  cooja in the background
./execute_cooja.sh | tee output.txt &

# Store the process ID (PID) of the first script
pid=$!

# Define a function to check for the desired output
check_output() {
    # Loop until the desired output is found
    while true; do
        # Check if the desired output is present in the script's output
        if grep -q "Simulation main loop started" output.txt; then
            # If found, break out of the loop
            break
        fi
        # Sleep for a while before checking again
        sleep 1
    done
}

# Call the check_output function
check_output

echo "SD-MIoT is ready"

# Once the desired output is found, execute the adaptor script
echo "Adaptor is starting"
./execute_adaptor.sh $HOSTNAME

# Optionally, you can wait for the first script to finish if needed
wait $pid
