FROM ubuntu:16.04

# installing required packages
# python2
RUN apt-get update && apt-get install -y clang libclang-3.5-dev python-clang-3.5 python-pip python-dev ant vim git sudo unzip
# python3
#RUN apt-get update && apt-get install -y clang libclang-3.5-dev python-clang-3.5 python3-pip python3-dev python3-tornado ant vim git sudo unzip
RUN sudo apt-get -y install openjdk-8-jdk 
RUN sudo apt-get -y install gcc-msp430

# enable regular user developer instead of root
# replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

# clone SD-MIoT repository
RUN git clone https://github.com/SWNRG/SD-MIoT /home/developer/SD-MIoT/

# make a soft link to contiki
RUN ln -s /home/developer/SD-MIoT/infrastructure-plane/contiki/ /home/developer/contiki

# enable serial2pty cooja plugin
WORKDIR /home/developer/contiki/tools/cooja/apps/serial2pty/
RUN rm -rf /home/developer/contiki/tools/cooja/apps/serial2pty
RUN git clone https://github.com/cmorty/cooja-serial2pty.git /home/developer/contiki/tools/cooja/apps/serial2pty
RUN echo "org.contikios.cooja.Cooja.PLUGINS = + de.fau.cooja.plugins.Serial2Pty" > /home/developer/contiki/tools/cooja/apps/serial2pty/cooja.config
RUN echo "org.contikios.cooja.Cooja.JARFILES = + serial2pty.jar" >> /home/developer/contiki/tools/cooja/apps/serial2pty/cooja.config
RUN echo "DESCRIPTION = serial2pty" >> /home/developer/contiki/tools/cooja/apps/serial2pty/cooja.config

# create up-to-date cooja jar file 
RUN JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/ ant jar

# enable serial2pty in cooja for user developer
ADD cooja.user.properties /home/developer/.cooja.user.properties
RUN sudo chown developer:developer /home/developer/.cooja.user.properties

# install node-red
RUN sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg apt-transport-https && \
    sudo mkdir -p /etc/apt/keyrings && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
    sudo echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list && \
    sudo apt-get update && sudo apt-get install nodejs -y && \
    sudo npm install -g --unsafe-perm node-red

# install node-red-dashboard
RUN mkdir /home/developer/.node-red
WORKDIR /home/developer/.node-red
RUN npm i node-red-dashboard

# install sd-miot custom flow
#RUN mkdir -p /home/developer/.node-red/public/topology
#ADD topology.zip /home/developer/
#RUN unzip /home/developer/topology.zip -d /home/developer/.node-red/public/
#RUN unzip /home/developer/topology.zip -d /home/developer/.node-red/
#RUN unzip /home/developer/SD-MIoT/application-plane/Dashboard/NodeRed-Topology-Source-v1.2.0.zip -d /home/developer/.node-red/public/topology/
RUN cp /home/developer/SD-MIoT/application-plane/Dashboard/SD-MIoT-Dashboard.json /home/developer/.node-red/flows.json
#ADD flows.json /home/developer/.node-red/

# replace localhost in node-red flow with correct host name
#RUN sed -i 's/"host": "localhost"/"host": "nephele-controller"/g' /home/developer/.node-red/flows.json
#RUN sed 's/\.\.\/topology\/img\//\/topology\/img\//g' /home/developer/.node-red/flows.json

# add SD-MIoT scenario
ADD nephele.csc /home/developer/
# fix file permissions
RUN sudo chown developer:developer /home/developer/nephele.csc

# upload startup scripts for cooja, adaptor and controller, as well as main startup script start.sh
ADD execute_cooja.sh /home/developer/
ADD execute_adaptor.sh /home/developer/
ADD execute_controller.sh /home/developer/
ADD execute_node-red.sh /home/developer/
ADD start.sh /home/developer/

# upload nephele wrapper to adapter
#ADD gateway_wrapper.py /home/developer/

# fix file permissions
RUN sudo chown developer:developer /home/developer/*.sh

# setting home directory as working directory 
WORKDIR /home/developer/

# executing main startup script
CMD /home/developer/start.sh
