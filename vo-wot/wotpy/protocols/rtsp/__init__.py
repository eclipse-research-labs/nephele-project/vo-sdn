#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
RTSP Protocol Binding implementation.

.. autosummary::
    :toctree: _rtsp

    wotpy.protocols.rtsp.enums
    wotpy.protocols.rtsp.server
"""
