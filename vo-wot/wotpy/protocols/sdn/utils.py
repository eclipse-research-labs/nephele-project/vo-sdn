#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Utility functions for SDN-related functionalities"""

import json

import tornado.httpclient

JSON_HEADERS = {"Content-Type": "application/json"}
XML_HEADERS = {"Content-Type": "application/xml"}

def inject_sdn_properties(exposed_thing, controller_url):
    """Inject SDN related properties to an exposed thing"""

#    async def flow_write_handler(value):
#        flow = json.dumps(value)
#        await exposed_thing._default_update_property_handler("sdn_flow", flow)
        #network = await exposed_thing.read_property("sdn_network")
        #if network is not None:
        #    schedule = await call_scheduler(scheduler_url, flow, network)
        #    if schedule is not None:
        #        await exposed_thing.write_property("netconf_schedule", schedule)

#    async def network_write_handler(value):
#        network = json.dumps(value)
#        await exposed_thing._default_update_property_handler("sdn_network", network)
        #flow = await exposed_thing.read_property("netconf_flow")
        #if flow is not None:
        #    schedule = await call_scheduler(scheduler_url, flow, network)
        #    if schedule is not None:
        #        await exposed_thing.write_property("netconf_schedule", schedule)

    #async def schedule_write_handler(value):
    #    await exposed_thing._default_update_property_handler("netconf_schedule", value)
    #    await call_netconf_server(netconf_server_url, value)

#    prop_dict = {
#        "type": "string",
#        "observable": True
#    }
#    sdn_property_handlers = {
#        "sdn_flow": flow_write_handler,
#        "sdn_network": network_write_handler
#    }
#    for proprty, handler in sdn_property_handlers.items():
        #exposed_thing.add_property(proprty, prop_dict)
#        exposed_thing.set_property_write_handler(proprty, handler)
