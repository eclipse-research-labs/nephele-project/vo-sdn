#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import random
import time

import pytest
from faker import Faker
from unittest.mock import MagicMock, patch

from tests.protocols.helpers import \
    client_test_on_property_change, \
    client_test_on_event, \
    client_test_read_property, \
    client_test_write_property, \
    client_test_invoke_action, \
    client_test_invoke_action_error
from tests.protocols.zenoh.router import is_test_router_online, ROUTER_SKIP_REASON
from tests.utils import run_test_coroutine, DEFAULT_TIMEOUT_SECS
from wotpy.protocols.exceptions import ClientRequestTimeout
from wotpy.protocols.zenoh.client import ZenohClient
from wotpy.wot.td import ThingDescription

pytestmark = pytest.mark.skipif(is_test_router_online() is False, reason=ROUTER_SKIP_REASON)


def test_read_property(zenoh_servient):
    """Property values may be retrieved using the Zenoh binding client."""

    client_test_read_property(zenoh_servient, ZenohClient)


def test_write_property(zenoh_servient):
    """Properties may be updated using the Zenoh binding client."""

    client_test_write_property(zenoh_servient, ZenohClient)


def test_invoke_action(zenoh_servient):
    """Actions may be invoked using the Zenoh binding client."""

    client_test_invoke_action(zenoh_servient, ZenohClient)


def test_invoke_action_error(zenoh_servient):
    """Errors raised by Actions are propagated propertly by the Zenoh binding client."""

    client_test_invoke_action_error(zenoh_servient, ZenohClient)


def test_on_property_change(zenoh_servient):
    """Property updates may be observed using the Zenoh binding client."""

    client_test_on_property_change(zenoh_servient, ZenohClient)


def test_on_event(zenoh_servient):
    """Event emissions may be observed using the Zenoh binding client."""

    client_test_on_event(zenoh_servient, ZenohClient)


# noinspection PyUnusedLocal
def _effect_dummy(*args, **kwargs):
    """Coroutine mock side effect that does nothing and returns a Mock."""

    def _coro():
        time.sleep(0)
        return MagicMock()

    return _coro()


def _build_effect_sleep(sleep_secs):
    """Factory function to build coroutine mock side effects to sleep a fixed amount of time."""

    # noinspection PyUnusedLocal
    def _effect_wait(*args, **kwargs):
        async def _coro():
            await asyncio.sleep(sleep_secs)
            raise asyncio.TimeoutError()

        return _coro()

    return _effect_wait


def _build_zenoh_mock():
    """Returns a mock of the Zenoh Session class."""

    mock_client = MagicMock()
    mock_client.connect.side_effect = _effect_dummy
    mock_client.close.side_effect = _effect_dummy
    mock_client.declare_subscriber.side_effect = _effect_dummy
    mock_client.put.side_effect = _effect_dummy

    mock_cls = MagicMock()
    mock_cls.return_value = mock_client

    return mock_cls


def test_timeout_invoke_action(zenoh_servient):
    """Timeouts can be defined on Action invocations."""

    exposed_thing = next(zenoh_servient.exposed_things)
    action_name = next(iter(exposed_thing.actions.keys()))
    td = ThingDescription.from_thing(exposed_thing.thing)
    zenoh_mock = _build_zenoh_mock()

    timeout = random.random()

    async def test_coroutine():
        with patch('wotpy.protocols.zenoh.client.zenoh.Session', new=zenoh_mock):
            zenoh_client = ZenohClient()

            with pytest.raises(ClientRequestTimeout):
                await zenoh_client.invoke_action(td, action_name, Faker().pystr(), timeout=timeout)

    run_test_coroutine(test_coroutine)


def test_timeout_read_property(zenoh_servient):
    """Timeouts can be defined on Property reads."""

    exposed_thing = next(zenoh_servient.exposed_things)
    prop_name = next(iter(exposed_thing.properties.keys()))
    td = ThingDescription.from_thing(exposed_thing.thing)
    zenoh_mock = _build_zenoh_mock()

    timeout = random.random()

    async def test_coroutine():
        with patch('wotpy.protocols.zenoh.client.zenoh.Session', new=zenoh_mock):
            zenoh_client = ZenohClient()

            with pytest.raises(ClientRequestTimeout):
                await zenoh_client.read_property(td, prop_name, timeout=timeout)

    run_test_coroutine(test_coroutine)


def test_timeout_write_property(zenoh_servient):
    """Timeouts can be defined on Property writes."""

    exposed_thing = next(zenoh_servient.exposed_things)
    prop_name = next(iter(exposed_thing.properties.keys()))
    td = ThingDescription.from_thing(exposed_thing.thing)
    zenoh_mock = _build_zenoh_mock()

    timeout = random.random()

    async def test_coroutine():
        with patch('wotpy.protocols.zenoh.client.zenoh.Session', new=zenoh_mock):
            zenoh_client = ZenohClient()

            with pytest.raises(ClientRequestTimeout):
                await zenoh_client.write_property(td, prop_name, Faker().pystr(), timeout=timeout)

    run_test_coroutine(test_coroutine)


def test_stop_timeout(zenoh_servient):
    """Attempting to stop an unresponsive connection does not result in an indefinite wait."""

    exposed_thing = next(zenoh_servient.exposed_things)
    prop_name = next(iter(exposed_thing.properties.keys()))
    td = ThingDescription.from_thing(exposed_thing.thing)

    timeout = random.random()

    assert (timeout * 3) < DEFAULT_TIMEOUT_SECS

    zenoh_mock = _build_zenoh_mock()

    async def test_coroutine():
        with patch('wotpy.protocols.zenoh.client.zenoh.Session', new=zenoh_mock):
            zenoh_client = ZenohClient(stop_loop_timeout_secs=timeout)

            with pytest.raises(ClientRequestTimeout):
                await zenoh_client.read_property(td, prop_name, timeout=timeout)

    run_test_coroutine(test_coroutine)
