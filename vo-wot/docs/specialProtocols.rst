.. _specialProtocols:

Special Protocols
=================

This page describes how Protocols that don't implement Protocol Bindings work. These protocols
don't host their server inside the VO but communicate with external servers instead.

NETCONF
-------

The NETCONF protocol can be used to interact with a scheduler and a NETCONF server. To do this the following section
has to be added to the VO descriptor:

.. code-block:: yaml

    bindingNB:
        # Netconf related configuration
        netconf:
            enabled: true
            # URL where the sheduler is running
            schedulerURL: "http://localhost:8080"
            # URL where the netconf server is running
            netconfServerURL: "https://localhost:8300"


After enabling the netconf protocol, the VO injects three special properties called ``netconf_flow``, ``netconf_network``
and ``netconf_schedule``. After the user writes values to the ``flow`` and ``network`` properties, the scheduler is invoked to
create a TAPRIO schedule. The schedule is then written to the ``netconf_schedule`` property and it is sent to the NETCONF
server.

RTSP
----

The RTSP protocol can be used to forward a stream from a device (e.g. a camera) to a local RTSP server.
This can be done by adding this block to the VO descriptor:

.. code-block:: yaml

    proxy:
        # Optional property that proxies the RTSP stream specified in the URL
        videoProperty: rtsp://example.com:8554/my-stream

During instantiation the VO will call ``fmpeg`` in a subprocess to read the stream from the specified URL and it will
forward it to a local RTSP server (accessible from the VO using localhost). To deploy a simple RTSP server, run:

.. code-block:: bash

    docker run --rm -it --network=host bluenviron/mediamtx

This will deploy an RTSP server locally and open TCP port 8554. Afterwards, the VO can be deployed and it will begin
forwarding the stream. Lastly, when fetching the Thing Description of the VO through its Catalogue Port, the
URL of the stream will be displayed in an injected property called ``video``.
