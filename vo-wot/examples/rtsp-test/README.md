# Simple example
This example takes an RTSP stream as input (e.g. from a device like a camera) and forwards it to a local RTSP server. The local RTSP server must be accessible at `localhost` from the VO. A simple way to run an RTSP server locally is by running:
```bash
docker run --rm -it --network=host bluenviron/mediamtx
```
By default this will deploy an RTSP server and open a few ports such as the TCP port 8554 for the RTSP protocol.
1. To run the example first create a virtual environment in python (the location of the virtual environment doesn't matter):
```bash
$ python -m venv vo-wot-env
```

2. Activate the environment:
```bash
$ source vo-wot-env/bin/activate
```

3. To install the local development version instead along with the `tests` and `docs` dependencies, you need to run:
```bash
(vo-wot-env)$ pip install -U -e .[tests,docs]
```

4. Run the example. The python package provides a cli named `vo-wot`. To use it run:
```bash
vo-wot/examples/rtsp-test(vo-wot-env)$ vo-wot -t td.json -f config.yaml app.py
```
Afterwards the Virtual Object will be running and be ready to serve requests.

5. Check the Thing Description served by the VO:
```bash
vo-wot/examples/rtsp-test(vo-wot-env)$ curl localhost:9090/vo-stream
```
This command should return a JSON Thing Description with a `video` property whose form has a URL with the RTSP stream.