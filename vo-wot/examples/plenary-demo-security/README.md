# Smaller demo with the VO1, cVO and app1 components
## Instructions to run the demo in minikube
For this demo a minikube installation of Kubernetes was used.
1. Depending on whether a minikube installation is used, build the appropriate image. Building the application 1 image automatically builds the base wotpy image. The Makefile inside the app1 directory has two rules: `app1` and `app1-minikube`. The second rule builds the image inside the docker daemon of minikube.
Run inside the `app-1` directory:
```bash
examples/plenary-demo-security/app-1$ make app1-minikube
```

2. Mount the `vo-1` and `cvo` directories to minikube. Run and leave the mount running.
For vo-1:
```bash
examples/plenary-demo-security$ cd vo-1
examples/plenary-demo-security/vo-1$ minikube mount $PWD:/mnt1
```
For the cvo:
```bash
examples/plenary-demo-security$ cd cvo
examples/plenary-demo-security/cvo$ minikube mount $PWD:/mnt2
```

3. Deploy the vo-1, cvo and app-1 by running:
```bash
examples/plenary-demo-security$ bash scripts/deploy-all.sh
```
This command will deploy the `vo-1`, `cvo` and `app-1`. Running `kubectl get pods` should return 3 pods: `app-1`, `cvo` and `vo1`. `app-1` and the `cvo` have 2 containers each (the app itself and the Holder component) while `vo1` has 4 containers (the VO , the Holder component, the Verifier component and the Proxy component).


4. The cVO and application 1 are exposed as NodePort Services. In the case that the minikube IP (can be fetched by running the command `minikube ip`) is `192.168.49.2`, the endpoints of the application 1 can be accessed through these URLs.
```bash
http://192.168.49.2:30030/temperature
```
The cVO `temperature` property serves as a proxy to the VO1 `temperature` property:
```bash
http://192.168.49.2:30080/humidity
```
