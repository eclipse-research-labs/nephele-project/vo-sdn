#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import logging
import requests
import json

from flask import Flask

from wotpy.wot.servient import Servient
from wotpy.wot.wot import WoT
from wotpy.protocols.http.client import HTTPClient

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

app = Flask(__name__)

SECURITY_SCHEME_DICT = {
    "scheme": "oidc4vp"
}
CREDENTIALS_DICT = {
    "holder_url": "http://localhost:8085",
    "requester": "app-1"
}

HOLDER_ISVCSTORED = "http://localhost:8085/isVCStored"

DEFAULT_VC_TEMPLATE= "NepheleId"

@app.route("/temperature")
def temperature():
    result = asyncio.run(temp())
    return f"<p>{result}</p>"

@app.route("/humidity")
def humidity():
    result = asyncio.run(hum())
    return f"<p>{result}</p>"

async def temp():
    checkVC()
    http_client = HTTPClient()
    http_client.set_security(SECURITY_SCHEME_DICT, CREDENTIALS_DICT)
    wot = WoT(servient=Servient(clients=[http_client]))
    consumed_thing = await wot.consume_from_url("http://vo1:9090/vo1", credentials_dict=CREDENTIALS_DICT)
    result = await consumed_thing.read_property("temperature")
    print(result)
    return result

async def hum():
    checkVC()
    http_client = HTTPClient()
    http_client.set_security(SECURITY_SCHEME_DICT, CREDENTIALS_DICT)
    wot = WoT(servient=Servient(clients=[http_client]))
    consumed_thing = await wot.consume_from_url("http://vo1:9090/vo1", credentials_dict=CREDENTIALS_DICT)
    result = await consumed_thing.read_property("humidity")
    print(result)
    return result

def requestVC(vcTemplate):
    print("Verifiable Credential not stored in Holder, starting process to obtain Verifiable Credential\n")
    attributes = {
        "uuid": "app-1",
        "type": "Application",
        "description": "Application deployed in UC#3 Energy Management scenario.",
        "vcTemplate": vcTemplate
    }
    attributesString = json.dumps(attributes)
    res = requests.post("http://localhost:8085/obtainCredential", json=attributes)
    if res.status_code == 200:
        print("Verifiable Credential Obtained!")
    else:
        print("There was an error obtaining the Verifiable Credential")

def checkVC():
    response = requests.get(HOLDER_ISVCSTORED)
    if response.status_code != 200:
        print("VC not stored, requesting VC...")
        #await requestVC(DEFAULT_VC_TEMPLATE)
        requestVC(DEFAULT_VC_TEMPLATE)
    else:
        print("There is already a VC stored")

if __name__ == "__main__":
    app.run(debug=True)
