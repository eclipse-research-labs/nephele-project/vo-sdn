import asyncio
import json
import uuid

from amqtt.client import MQTTClient
from amqtt.mqtt.constants import QOS_1, QOS_2


def create_read_payload():
    return {
        "action": "read"
    }


def create_write_payload(input):
    return {
        "action": "write",
        "value": str(input),
        "ack": uuid.uuid4().hex
    }


async def main():
    C = MQTTClient()
    await C.connect("mqtt://localhost:1883/")

    ###################### Property write ########################
    # These two tasks will write the values for the temperature
    # and the humidity properties
    await asyncio.gather(
        asyncio.ensure_future(
            C.publish(
                "wotpy/property/requests/vo/temperature",
                json.dumps(create_write_payload(100)).encode('utf-8')
            )
        ),
        asyncio.ensure_future(
            C.publish(
                "wotpy/property/requests/vo/humidity",
                json.dumps(create_write_payload(70)).encode('utf-8')
            )
        )
    )
    ##############################################################

    ###################### Property read ########################
    # These two topics are used to receive the property read replies
    await C.subscribe(
        [
            ("wotpy/property/updates/vo/temperature", QOS_1),
            ("wotpy/property/updates/vo/humidity", QOS_2),
        ]
    )

    # These two publish tasks request an update and trigger the VO
    # to write in the above topics
    await asyncio.gather(
        asyncio.ensure_future(
            C.publish(
                "wotpy/property/requests/vo/temperature",
                json.dumps(create_read_payload()).encode('utf-8')
            )
        ),
        asyncio.ensure_future(
            C.publish(
                "wotpy/property/requests/vo/humidity",
                json.dumps(create_read_payload()).encode('utf-8')
            )
        )
    )

    # Afterwards we consume the read property values
    for i in range(2):
        message = await C.deliver_message()
        packet = message.publish_packet
        print(
            "%d: %s => %s"
            % (i, packet.variable_header.topic_name, str(packet.payload.data))
        )
    await C.disconnect()
    ##############################################################


if __name__ == "__main__":
    asyncio.run(main())
