# MQTT device test
This scenario regards a constrained device that can only send data through MQTT. The corresponding VO connects to the same MQTT broker as the device through its NorthBound interface. Afterward, the device sends data to the VO using specific topics.

This means that the communication is unidirectional:
Device ---> Virtual Object

1. Start an MQTT broker
You can run a mosquitto broker using the `mosquitto` directory:
```bash
examples/mqtt-device/mosquitto$ bash run-mosquitto.sh
```
This will run a mosquitto broker in detached mode at `localhost` at port `1883` and will allow anonymous connections.

2. Start the Virtual Object
To run the VO first install the python package. In the root directory of the repository do:
```bash
$ python -m venv .venv
$ source .venv/bin/activate
(venv)$ pip install -U -e .[tests,docs]
```
Afterwards in this directory do:
```bash
examples/mqtt-device(venv)$ vo-wot -f vo.yaml -t vo_td.json vo.py
```
The VO should be running and waiting for requests that come to the MQTT broker.

3. Read and write to the VO:
To mock a device and send data to the VO run the `mqtt_send.py` file in a different terminal:
```bash
examples/mqtt-device(venv)$ python mqtt_send.py
```
The above python script performs two operations. Writing properties and subsequently reading the written values.
To do that, it uses specific topics that VO already listens to:
1. **Write**: After writing to the `requests` topic of the VO, the VO consumes the written values and updates its internal state.
2. **Read**: After subscribing to the property `updates` topic, the device writes to the `requests` topic and triggers the read
of a property's value. The update is then fetched through the `updates` topic.

