#!/bin/bash

# How to run testing script 
# ------------------------------------------------------
# Execute VO-SDN in a shell window:
# cd nephele-sdiot/vo-wot/examples/vo-sdn
# source vo-wot-env/bin/activate
# ./executeVO.sh

# Execute IoT network topology emulator in another shell window:
# cd nephele-sdiot
# docker compose up

# Then you can execute test.sh in a third window:
# ./test.sh

# send SDN configuration
echo "Sending SDN configuration to VO-SDN"
cat sdn_configuration.json | jq
./send_configuration.sh

# wait for topology to be established
echo "Wait for topology to be established"
sleep 10

# retrieve SDN topology
echo "Retrieve SDN topology from VO-SDN"
./get_topology.sh
echo ""

# wait a bit
echo "Wait a bit"
sleep 1

# send SDN flows (establishment of flows is under construction)
echo "Send SDN flows to VO-SDN"
cat sdn_flows.json | jq
./send_flows.sh
