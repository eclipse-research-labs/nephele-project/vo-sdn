import json
import tornado.httpclient

JSON_HEADERS = {"Content-Type": "application/json"}
XML_HEADERS = {"Content-Type": "application/xml"}

async def apply_sdn_configuration (controller_url, sdn_configuration):
    print ("connecting to controller URL to apply SDN configuration")

    api_url = f"{controller_url}/configuration"
    print('controller URL :::', api_url, flush=True)
    http_client = tornado.httpclient.AsyncHTTPClient()
    http_request = tornado.httpclient.HTTPRequest(
        api_url,
        method="POST",
        headers=JSON_HEADERS,
        body=json.dumps(sdn_configuration),
        allow_nonstandard_methods=True
    )
    try:
        response = await http_client.fetch(http_request)
        return json.loads(response.body)

    except tornado.httpclient.HTTPError as e:
        print(f"HTTP error occurred: {e}")
        if e.response:
            return {"error": f"Failed to apply configuration: {e.response.body}"}
        return {"error": f"HTTPError: {str(e)}"}

    except Exception as e:
        print(f"An error occurred: {e}")
        return {"error": f"An unexpected error occurred: {str(e)}"}

async def sdn_configuration_write_handler(value):
    print ("SDN configuration json received:", value)
    await exposed_thing._default_update_property_handler("sdn_configuration", value)
    sdn_configuration = await exposed_thing.read_property("sdn_configuration")
    if sdn_configuration is not None:
        controller_url = exposed_thing.servient.config["bindingNB"]["sdn"]["controllerURL"]
        result = await apply_sdn_configuration(controller_url, sdn_configuration)
        if result is not None:
            print (result)
            #await exposed_thing.write_property("sdn_topology", sdn_topology)

async def retrieve_sdn_topology (controller_url):
    print ("connecting to controller URL to retrieve SDN topology")

    api_url = f"{controller_url}/topology"
    print('controller URL :::', api_url, flush=True)
    http_client = tornado.httpclient.AsyncHTTPClient()
    http_request = tornado.httpclient.HTTPRequest(
        api_url,
        method="GET",
        headers=JSON_HEADERS,
        #body=json.dumps(sdn_configuration),
        allow_nonstandard_methods=True
    )
    response = await http_client.fetch(http_request)
    return json.loads(response.body)

async def sdn_topology_read_handler():
    print ("Retrieving sdn topological information.")
    # connect to SDN controller
    controller_url = exposed_thing.servient.config["bindingNB"]["sdn"]["controllerURL"]
    sdn_topology = await retrieve_sdn_topology (controller_url)
    # store property in VO
    await exposed_thing._default_update_property_handler("sdn_topology", sdn_topology)
    #sdn_topology = await exposed_thing._default_retrieve_property_handler("sdn_topology")
    return sdn_topology

async def apply_sdn_flows (controller_url, sdn_flows):
    print ("connecting to controller URL to apply SDN flows")

    api_url = f"{controller_url}/flows"
    print('controller URL :::', api_url, flush=True)
    http_client = tornado.httpclient.AsyncHTTPClient()
    http_request = tornado.httpclient.HTTPRequest(
        api_url,
        method="POST",
        headers=JSON_HEADERS,
        body=json.dumps(sdn_flows),
        allow_nonstandard_methods=True
    )
    response = await http_client.fetch(http_request)
    # Print raw response body
    print("Raw response body:", response.body.decode())
    return json.loads(response.body)

async def sdn_flows_write_handler(value):
    print ("SDN flows json received:", value)
    await exposed_thing._default_update_property_handler("sdn_flows", value)
    sdn_flows = await exposed_thing.read_property("sdn_flows")
    if sdn_flows is not None:
        controller_url = exposed_thing.servient.config["bindingNB"]["sdn"]["controllerURL"]
        print (sdn_flows)

        result = await apply_sdn_flows(controller_url, sdn_flows)
        if result is not None:
            print (result)
